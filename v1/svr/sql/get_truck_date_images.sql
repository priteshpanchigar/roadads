DROP PROCEDURE IF EXISTS get_truck_date_images;
DELIMITER $$
CREATE  PROCEDURE `get_truck_date_images`(IN intruckno INT, IN intruckuniquekey varchar(225))
BEGIN

SELECT truck_image_id, a.formatted_address, truck_no, t.truck_unique_key, image_name, image_path, appuser_id,
image_creationd_datetime, ti.address_id, `delete`, ti.uniquekey, a.latitude, a.longitude, a.locality
FROM truck t INNER JOIN truck_image ti
ON t.truck_unique_key = ti.truck_unique_key
INNER JOIN address a on ti.address_id = a.address_id
WHERE  t.truck_no = intruckno 
AND t.truck_unique_key = intruckuniquekey 
AND `delete` = 0
ORDER BY truck_image_id DESC;



END$$
DELIMITER;

call get_truck_date_images(5248, '2016-05-06');