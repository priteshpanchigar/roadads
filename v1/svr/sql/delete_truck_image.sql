DROP PROCEDURE IF EXISTS delete_truck_image;
DELIMITER $$
CREATE  PROCEDURE `delete_truck_image`(IN intruckimageid INT)
BEGIN

UPDATE truck_image
	SET `delete` = 1
	 WHERE	truck_image_id = intruckimageid;
	 
	Select 1 as result;

END$$
DELIMITER;

call delete_truck_image(1);