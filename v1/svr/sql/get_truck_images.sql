DROP PROCEDURE IF EXISTS get_truck_images;
DELIMITER $$
CREATE  PROCEDURE `get_truck_images`()
BEGIN
 
SELECT COUNT(truck_no), b.truck_datetime,truck_image_id,  b.truck_no, a.uniquekey, ad.formatted_address,ad.locality,
ad.latitude, ad.longitude,
a.truck_unique_key, image_name, image_path, image_creationd_datetime, a.address_id,
 `delete` FROM truck_image a INNER JOIN truck b
ON a.truck_unique_key = b.truck_unique_key 
INNER JOIN address ad ON a.address_id = ad.address_id
	WHERE `delete` = 0 
GROUP BY truck_unique_key,  DATE(image_creationd_datetime)
ORDER BY a.truck_image_id DESC;



END$$
DELIMITER;

call get_truck_images();