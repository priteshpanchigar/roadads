DROP PROCEDURE IF EXISTS add_truck_no;
DELIMITER $$
CREATE  PROCEDURE `add_truck_no`( IN intruckno INT, IN intruckuniquekey varchar(50),
IN inclientdatetime datetime, IN inappuserid INT)
BEGIN
DECLARE vTruckID int;

INSERT INTO truck(truck_no, truck_unique_key, truck_datetime, appuser_id)
VALUES (intruckno, intruckuniquekey, inclientdatetime, inappuserid);
		
SELECT LAST_INSERT_ID() INTO vTruckID;


SELECT CASE WHEN vTruckID IS NULL THEN -1 ELSE vTruckID  END truck_id;

END$$
DELIMITER;

call add_truck_no(1165,'pasdqrst',"2016-05-02 25:28:38",7);