DROP PROCEDURE IF EXISTS truck_image;
DELIMITER $$
CREATE  PROCEDURE `truck_image`(IN inimagename varchar(255), IN inclientdatetime datetime, IN inimagepath varchar(255), IN intruckuniquekey varchar(50),IN informattedaddress varchar(255), IN inlocality varchar(255),  IN  inlatitude double, IN inlongitude double, IN inuniquekey varchar(255))
quit_proc: BEGIN
IF inimagename IS NULL THEN
LEAVE quit_proc;
END IF;
BEGIN

DECLARE vTruckImageID int;
DECLARE vaddress_id int;

IF informattedaddress IS NOT NULL THEN
SELECT address_id INTO vaddress_id from address
where latitude = inlatitude AND
longitude = inlongitude LIMIT 1;
END IF;


IF (vaddress_id IS NULL) THEN
		INSERT INTO address(formatted_address, locality, client_datetime,latitude,longitude)
		VALUES(informattedaddress,inlocality, inclientdatetime, inlatitude, inlongitude);
 SELECT LAST_INSERT_ID() INTO vaddress_id from address  LIMIT 1;
ELSE
	UPDATE address
		SET locality = inlocality
					WHERE formatted_address = informattedaddress;
END IF;


INSERT INTO truck_image(truck_unique_key, image_name, image_path, image_creationd_datetime, address_id, uniquekey )
VALUES (intruckuniquekey, inimagename, inimagepath, inclientdatetime,  vaddress_id, inuniquekey)
ON DUPLICATE KEY UPDATE
		image_name = inimagename;

	
SELECT LAST_INSERT_ID() INTO vTruckImageID;


SELECT CASE WHEN vTruckImageID IS NULL THEN -1 ELSE vTruckImageID  END truck_image_id;
END;	
END$$
DELIMITER;


