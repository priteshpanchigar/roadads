
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `formatted_address` varchar(255) DEFAULT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `client_datetime` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `od_formatted_address` (`formatted_address`) USING BTREE,
  KEY `address_id` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appusage
-- ----------------------------
DROP TABLE IF EXISTS `appusage`;
CREATE TABLE `appusage` (
  `appusage_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) NOT NULL DEFAULT '0',
  `clientfirstaccessdatetime` datetime DEFAULT NULL,
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `serverfirstaccessdatetime` datetime DEFAULT NULL,
  `serverlastaccessdatetime` datetime DEFAULT NULL,
  `versioncode` int(6) DEFAULT NULL,
  `version` varchar(25) DEFAULT NULL,
  `app` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`appusage_id`),
  UNIQUE KEY `appuser_id` (`appuser_id`,`app`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appuser
-- ----------------------------
DROP TABLE IF EXISTS `appuser`;
CREATE TABLE `appuser` (
  `appuser_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL DEFAULT '',
  `imei` varchar(50) NOT NULL DEFAULT '',
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `first_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  `provider` varchar(10) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `product` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `android_release_version` varchar(20) DEFAULT NULL,
  `android_sdk_version` int(5) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `useragent` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`appuser_id`),
  UNIQUE KEY `app_email_imei` (`email`,`imei`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `location_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` int(11) DEFAULT NULL,
  `altitude` double DEFAULT NULL,
  `bearing` float DEFAULT NULL,
  `firsttime` datetime DEFAULT NULL,
  `loctime` datetime DEFAULT NULL,
  `clientaccessdatetime` datetime DEFAULT NULL,
  `speed` float DEFAULT NULL,
  `cumulative_distance` decimal(5,3) DEFAULT NULL,
  `provider` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for truck
-- ----------------------------
DROP TABLE IF EXISTS `truck`;
CREATE TABLE `truck` (
  `truck_id` int(11) NOT NULL AUTO_INCREMENT,
  `truck_no` int(5) DEFAULT NULL,
  `truck_unique_key` varchar(255) DEFAULT NULL,
  `truck_datetime` datetime DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`truck_id`),
  UNIQUE KEY `truck_unique` (`truck_no`,`truck_datetime`),
  KEY `truck_no` (`truck_no`),
  KEY `truck_unique_key` (`truck_unique_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for truck_image
-- ----------------------------
DROP TABLE IF EXISTS `truck_image`;
CREATE TABLE `truck_image` (
  `truck_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `truck_unique_key` varchar(50) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_creationd_datetime` datetime DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `delete` int(11) DEFAULT '0',
  `uniquekey` varchar(255) NOT NULL,
  PRIMARY KEY (`truck_image_id`),
  UNIQUE KEY `unique_creation_datetime` (`image_creationd_datetime`) USING BTREE,
  KEY `truck_unique_key` (`truck_unique_key`),
  KEY `address_id` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
