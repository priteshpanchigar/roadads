<?php
header('Access-Control-Allow-Origin: *');	
error_reporting(E_ALL);
if(empty($php_name)) {
	$php_name = "variables";
}
/* variables example

//for default 0 AND INT
$myissue = empty($_REQUEST['myissue']) || !isset($_REQUEST['myissue']) ? 0 : $_REQUEST['myissue'];

//for default NULL AND INT
$minlng = empty($_REQUEST['minlng']) || !isset($_REQUEST['minlng']) ? 'NULL' : $_REQUEST['minlng'];

//for default NULL AND VARCHAR
$RS = empty($_REQUEST['RS']) || !isset($_REQUEST['RS']) ? 'NULL' :
		"'" . $_REQUEST['RS'] . "'" ;
*/

$verified = empty($_REQUEST['verified']) || !isset($_REQUEST['verified']) ? 0 : $_REQUEST['verified'];
$flagname = empty($_REQUEST['flagname']) || !isset($_REQUEST['flagname']) ? 'NULL' :
		"'" . $_REQUEST['flagname'] . "'" ;
$flagvalue = empty($_REQUEST['flagvalue']) || !isset($_REQUEST['flagvalue']) ? 0 : $_REQUEST['flagvalue'];

$triptype = empty($_REQUEST['triptype']) || !isset($_REQUEST['triptype']) ? 'NULL' :
		"'" . $_REQUEST['triptype'] . "'" ;
$fromaddress = empty($_REQUEST['fromaddress']) || !isset($_REQUEST['fromaddress']) ? 'NULL' :
		"'" . $_REQUEST['fromaddress'] . "'" ;
$fromshortaddress = empty($_REQUEST['fromshortaddress']) || !isset($_REQUEST['fromshortaddress']) ? 'NULL' :
		"'" . $_REQUEST['fromshortaddress'] . "'" ;
$toaddress = empty($_REQUEST['toaddress']) || !isset($_REQUEST['toaddress']) ? 'NULL' :
		"'" . $_REQUEST['toaddress'] . "'" ;
$toshortaddress = empty($_REQUEST['toshortaddress']) || !isset($_REQUEST['toshortaddress']) ? 'NULL' :
		"'" . $_REQUEST['toshortaddress'] . "'" ;
$fromsublocality = empty($_REQUEST['fromsublocality']) || !isset($_REQUEST['fromsublocality']) ? 'NULL' :
		"'" . $_REQUEST['fromsublocality'] . "'" ;
$tosublocality = empty($_REQUEST['tosublocality']) || !isset($_REQUEST['tosublocality']) ? 'NULL' :
		"'" . $_REQUEST['tosublocality'] . "'" ;
$trip_directions_polyline = empty($_REQUEST['trip_directions_polyline']) || !isset($_REQUEST['trip_directions_polyline']) ? 'NULL' :
		"'" . $_REQUEST['trip_directions_polyline'] . "'" ;
$fromlat = empty($_REQUEST['fromlat']) || !isset($_REQUEST['fromlat']) ? 'NULL' : $_REQUEST['fromlat'];
$fromlng = empty($_REQUEST['fromlng']) || !isset($_REQUEST['fromlng']) ? 'NULL' : $_REQUEST['fromlng'];
$tripaction = empty($_REQUEST['tripaction']) || !isset($_REQUEST['tripaction']) ? 'NULL' :
		"'" . $_REQUEST['tripaction'] . "'" ;
$triptime = empty($_REQUEST['triptime']) || !isset($_REQUEST['triptime']) ? 'NULL' : $_REQUEST['triptime'];
$tripdistance = empty($_REQUEST['tripdistance']) || !isset($_REQUEST['tripdistance']) ? 'NULL' : $_REQUEST['tripdistance'];
$plannedstartdatetime = empty($_REQUEST['plannedstartdatetime']) || !isset($_REQUEST['plannedstartdatetime']) ? 'NULL' :
		"'" . $_REQUEST['plannedstartdatetime'] . "'" ;
$tripid = empty($_REQUEST['tripid']) || !isset($_REQUEST['tripid']) ? -1 : $_REQUEST['tripid'];
$vehicleno = empty($_REQUEST['vehicleno']) || !isset($_REQUEST['vehicleno']) ? 'NULL' : $_REQUEST['vehicleno'];
$vehicleid = empty($_REQUEST['vehicleid']) || !isset($_REQUEST['vehicleid']) ? 'NULL' : $_REQUEST['vehicleid'];
$shiftid = empty($_REQUEST['shiftid']) || !isset($_REQUEST['shiftid']) ? 'NULL' : $_REQUEST['shiftid'];

$tolat = empty($_REQUEST['tolat']) || !isset($_REQUEST['tolat']) ? 'NULL' : $_REQUEST['tolat'];
$tolng = empty($_REQUEST['tolng']) || !isset($_REQUEST['tolng']) ? 'NULL' : $_REQUEST['tolng'];

$intrip_action = empty($_REQUEST['tripaction']) || !isset($_REQUEST['tripaction']) ? 'NULL' :
		"'" . $_REQUEST['tripaction'] . "'" ;
$servicecode = empty($_REQUEST['servicecode']) || !isset($_REQUEST['servicecode']) ? 'NULL' :
		"'" . $_REQUEST['servicecode'] . "'" ;
$messagetopost = empty($_REQUEST['messagetopost']) || !isset($_REQUEST['messagetopost']) ? 'NULL' :
		"'" . $_REQUEST['messagetopost'] . "'" ;
$tripcost = empty($_REQUEST['tripcost']) || !isset($_REQUEST['tripcost']) ? 'NULL' : $_REQUEST['tripcost'];

$triprating = empty($_REQUEST['triprating']) || !isset($_REQUEST['triprating']) ? 'NULL' : $_REQUEST['triprating'];	

$tripcomment = empty($_REQUEST['tripcomment']) || !isset($_REQUEST['tripcomment']) ? 'NULL' :
		"'" . $_REQUEST['tripcomment'] . "'" ;		