<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'deleteTruckImages';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_short.php");

if ($mysqli){
	
	$truckimageid = empty($_REQUEST['truckimageid']) || !isset($_REQUEST['truckimageid']) ? 'NULL' : $_REQUEST['truckimageid'];
	
	$sql = "call delete_truck_image(".$truckimageid . ")";
	
	if ($verbose != 'N') {
		echo $sql . '<br>' ;
	}
	
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		} 
		
	}		else {
		echo json_encode((object) null); // something went wrong, probably sql failed
	}
	$mysqli->close();
} else {
	echo "-2"; // "Connection to db failed";
}