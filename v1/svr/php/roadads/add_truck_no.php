<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'addTruckNo';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_sar.php");
if ($mysqli){
	
	$truckno = empty($_REQUEST['truckno']) || !isset($_REQUEST['truckno']) ? 'NULL' : $_REQUEST['truckno'];
	
	$truckuniquekey = empty($_REQUEST['truckuniquekey']) || !isset($_REQUEST['truckuniquekey']) ? 'NULL' :
		"'" . $_REQUEST['truckuniquekey'] . "'" ;
	
	 $creationdatetime = empty($_REQUEST['creationdatetime']) || !isset($_REQUEST['creationdatetime']) ? 'NULL' :
		"'".$_REQUEST['creationdatetime']."'";
	
	$sql = "call add_truck_no(".$truckno . ",".$truckuniquekey . ",".$creationdatetime ."," .$appuserid . ")";
	
	if ($verbose != 'N') {
		echo $sql . '<br>' ;
	}  
	
	if ($result = $mysqli->query($sql)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		} 
		
	}		else {
		echo json_encode((object) null); // something went wrong, probably sql failed
	}
	$mysqli->close();
} else {
	echo "-2"; // "Connection to db failed";
}