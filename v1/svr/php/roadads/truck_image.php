<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'truckImage';
include("../dbconn_sar_apk.php");  
include("../mobile_common_data_sar.php");

$imagefilename = NULL;
$imsrc         = '';
$creationdatetime='';

if (isset($_REQUEST['imagefilename']) && !empty($_REQUEST['imagefilename'])) {
    $imagefilename = $_REQUEST['imagefilename'];
    if ($verbose != 'N') {
        echo '<br>imagefilename: ' . $_REQUEST['imagefilename'];
    }
	$imsrc = base64_decode($_REQUEST['image']);
	
    $fp    = fopen('image/' . $imagefilename, 'w');
     
    fwrite($fp, $imsrc);
    if (fclose($fp)) {
        echo "Image uploaded";
    } else {
        echo "Error uploading image";
    }
    
} else {
    $imagefilename = 'NULL';
}

if ($mysqli) {
    
    $imagepath     =  "'/v1/svr/php/roadads/images/'";
	
	$truckuniquekey = empty($_REQUEST['truckuniquekey']) || !isset($_REQUEST['truckuniquekey']) ? 'NULL' :
		"'" . $_REQUEST['truckuniquekey'] . "'" ;
	$formattedaddress = empty($_REQUEST['formattedaddress']) || !isset($_REQUEST['formattedaddress']) ? 'NULL' :
		"'" . $_REQUEST['formattedaddress'] . "'" ;
		
	$locality = empty($_REQUEST['locality']) || !isset($_REQUEST['locality']) ? 'NULL' :
		"'" . $_REQUEST['locality'] . "'" ;  
		
	$latitude = empty($_REQUEST['latitude']) || !isset($_REQUEST['latitude']) ? 'NULL' : $_REQUEST['latitude'];

	$longitude = empty($_REQUEST['longitude']) || !isset($_REQUEST['longitude']) ? 'NULL' : $_REQUEST['longitude'];
        
    $imagefilename = empty($_REQUEST['imagefilename']) || !isset($_REQUEST['imagefilename']) ? 'NULL' :
		"'" . $_REQUEST['imagefilename'] . "'";   
	
	$truckimageid = empty($_REQUEST['truckimageid']) || !isset($_REQUEST['truckimageid']) ? 'NULL' :
		$_REQUEST['truckimageid'];
	 $creationdatetime = empty($_REQUEST['creationdatetime']) || !isset($_REQUEST['creationdatetime']) ? 'NULL' :
		"'".$_REQUEST['creationdatetime']."'";
    $uniquekey = empty($_REQUEST['uniquekey']) || !isset($_REQUEST['uniquekey']) ? 'NULL' :
		"'" . $_REQUEST['uniquekey'] . "'" ;
	    
    $sql = "call truck_image(" . $imagefilename .  ", " . $creationdatetime .  ", " . $imagepath . 
							 ",".$truckuniquekey .  "," .$formattedaddress ."," .$locality  .
							"," .$latitude ."," .$longitude . "," . $uniquekey .")";
    
    if ($verbose != 'N') {
        echo '<br>sql:<br>' . $sql;
    }
    if ($result = $mysqli->query($sql)) {
        if ($result && is_object($result)) {
            while ($row = $result->fetch_assoc()) {
                echo json_encode($row);
                break;
            }
        }
        
    }
    $mysqli->close();
} else {
    echo "-1";
}