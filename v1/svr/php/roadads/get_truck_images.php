<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getTruckImages';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli) {
    $truckimagesRows = array();
    
    $sql = " call get_truck_images()";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
    if ($result = $mysqli->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $truckimagesRows[] = $row;
        }
        $result->free(); // free result set
    }
    
    
    $truckimagesRows = array_filter($truckimagesRows);
    if (!empty($truckimagesRows)) {
        echo json_encode($truckimagesRows);
    } else {
        echo "-1";
    }    
    $mysqli->close(); // close connection
} else {
    echo "-1";
}