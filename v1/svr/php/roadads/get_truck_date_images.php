<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getTruckDateImages';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");   
if ($mysqli) {
    $truckdateimagesRows = array();
    
	$truckno = empty($_REQUEST['truckno']) || !isset($_REQUEST['truckno']) ? 'NULL' : $_REQUEST['truckno'];
	
	$truckuniquekey = empty($_REQUEST['truckuniquekey']) || !isset($_REQUEST['truckuniquekey']) ? 'NULL' :
		"'" . $_REQUEST['truckuniquekey'] . "'" ; 
	
    $sql = " call get_truck_date_images(".$truckno. ',' .$truckuniquekey.")";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
    if ($result = $mysqli->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $truckdateimagesRows[] = $row;
        }
        $result->free(); // free result set
    }
    
    
    $truckdateimagesRows = array_filter($truckdateimagesRows);
    if (!empty($truckdateimagesRows)) {
        echo json_encode($truckdateimagesRows);
    } else {
        echo "-1";
    }    
   $mysqli->close(); // close connection 
} else {
    echo "-1";
}